//
//  DetailViewController.h
//  TableViewDemo
//
//  Created by James Cash on 22-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailViewController : UIViewController

@property (nonatomic,strong) NSString *detailData;

@end

NS_ASSUME_NONNULL_END
