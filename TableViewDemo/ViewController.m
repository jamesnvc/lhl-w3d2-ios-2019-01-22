//
//  ViewController.m
//  TableViewDemo
//
//  Created by James Cash on 22-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "GreenTableViewCell.h"
#import "DetailViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic,strong) NSArray<NSArray<NSString*>*>* data;
@property (nonatomic,weak) IBOutlet UITableView* tableView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.data = @[@[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"],
                  @[@"a", @"b", @"c"],
                  @[@"d", @"e", @"f", @"g"],
                  @[@"foo", @"bar", @"baz"]
                  ];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showDetailSegue"]) {
        // pass info about selected row?
        // Method 1 of getting data:
        /*
        NSIndexPath *idx = [self.tableView indexPathForSelectedRow];
        NSString *info = self.data[idx.section][idx.row];
         */
        // Method 2 of getting data:
        // because the segue was dragged from the tableviewcell, the cell itself is going to be the sender
        GreenTableViewCell *cell = sender;
        NSString *info = cell.data;

        DetailViewController* dvc = segue.destinationViewController;
        // THIS IS WRONG:
        // at this point, the dvc's view hasn't been loaded yet
        // so the detailLabel is nil
//        dvc.detailLabel.text = info;
        // instead of setting the view (which is kind of bad style anyway), we just pass the data to the dvc & let it configure its views when it's ready
        dvc.detailData = info;
        

    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.data count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GreenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"greenCell"];

    if (indexPath.row == 0 && (indexPath.section == 0 || indexPath.section == 5)) {
        cell.extraText = @"EXTRA";
    }

    // Customize cell here
    NSString *s = self.data[indexPath.section][indexPath.row];
    [cell configureCell:s indexPath:indexPath];

    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44 + 25 * indexPath.row;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"SELECTED %@", indexPath);
    // you could also manually triger segue and pass the data object directly as the sender
//    [self performSegueWithIdentifier:@"showDetailSegue" sender:self.data[indexPath.section][indexPath.row]];
    // This below is equivalent to what we did in the storyboard
    /*
    GreenTableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"showDetailSegue" sender:selectedCell];
     */
}

@end
