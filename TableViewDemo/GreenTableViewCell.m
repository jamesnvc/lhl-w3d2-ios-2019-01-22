//
//  GreenTableViewCell.m
//  TableViewDemo
//
//  Created by James Cash on 22-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "GreenTableViewCell.h"

@interface GreenTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *theLabel;
@end

@implementation GreenTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// cell has already been created, but is about to be re-used for a different location
// this is our chance to reset to a clean slate
- (void)prepareForReuse
{
    [super prepareForReuse];
    self.extraText = nil;
}

- (void)configureCell:(NSString *)data indexPath:(NSIndexPath *)indexPath
{
    self.data = data;
    self.theLabel.text = data;
    if (self.extraText) {
        self.theLabel.text = [self.theLabel.text stringByAppendingString:self.extraText];
    }
    if (indexPath.section % 2 == 0) {
        self.contentView.backgroundColor = [UIColor redColor];
    } else {
        self.contentView.backgroundColor = [UIColor greenColor];
    }
}

@end
