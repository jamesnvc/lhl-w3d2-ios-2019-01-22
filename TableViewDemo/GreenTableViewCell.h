//
//  GreenTableViewCell.h
//  TableViewDemo
//
//  Created by James Cash on 22-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GreenTableViewCell : UITableViewCell

- (void)configureCell:(NSString*)data indexPath:(NSIndexPath*)indexPath;
@property (nonatomic,strong,nullable) NSString* extraText;
@property (nonatomic,strong) NSString* data;

@end

NS_ASSUME_NONNULL_END
